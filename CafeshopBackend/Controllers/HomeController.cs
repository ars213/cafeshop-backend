﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CafeshopBackend.Models;

namespace CafeshopBackend.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            var users = new CafeshopEntities().Users.ToList();
            return View(users);
        }
    }
}
